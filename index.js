'use strict';

// Iniciando base
var base     =  {
                    sequence: 0,
                    rules: [],
                    days: [],
                    0: [],
                    1: [],
                    2: [],
                    3: [],
                    4: [],
                    5: [],
                    6: []
                };
// Acesso a biblioteca para manipulacao de arquivos
var fs       = require('fs');
// Nome do arquivo da base
var fileName = 'base.json';

// Carregando base caso ela exista
if (fs.existsSync(fileName)) {
    fs.readFile(fileName, (err, data)  =>  {
        if (err) throw err;    
        base = data.length ? JSON.parse(data) : base;
    });
}

// Metodo para salvar a base
function saveBase(base) {
    base.days.sort(function(a, b){
        return new Date(a.day) - new Date(b.day);
    });
    // Gravar Arquivo
    fs.writeFile(fileName, JSON.stringify(base), (err) => {
        if (err) throw err;
    });
}

// Metodo para validar uma data
function isValidDate(date) {
    let datePart = date.split('/');
    let d = new Date(datePart[2] + '/' + datePart[0] + '/' + datePart[1]);
    return !!(d && (d.getMonth() + 1) == datePart[0] && d.getDate() == Number(datePart[1]));
}

// Metodo para carregar as regras nos dias da semana
function loadWeekIntervals(rule) {
    let c = 0;
    if (rule.weekdays) {        
        while (c < rule.weekdays.length) {
            base[rule.weekdays[c]] = base[rule.weekdays[c]].concat(rule.intervals);
            c++;
        }
    } else {
        for (; c < 7; c++) {
            base[c] = base[c].concat(rule.intervals);
        }
    }
}

// Metodo para remover 
function removeRuleFromWeek(weekDays, intervals) {
    let c = 0;    
    let i = 0;  
    let j = 0;  
    let flag = false;
    while (c < weekDays.length) {
        i = 0;
        while (i < intervals.length) {
            j = 0;        
            flag = false;    
            while (j < base[weekDays[c]].length && !flag) {
                if (base[weekDays[c]][j].start == intervals[i].start && base[weekDays[c]][j].end == intervals[i].end) {
                    base[weekDays[c]].splice(j, 1);
                    flag = true;
                }
                j++;
            }
            i++;
        }
        c++;
    }   
    return flag;     
}

// Metodo para remover regra
function removeRule(id) {    
    let flag  = false;
    let rule;
    let c = 0;
    while (c < base.rules.length && !flag) {
        if (base.rules[c].id == id) {
            rule = base.rules.splice(c, 1);
            flag = true;            
        }
        c++;
    }
    if (flag && rule[0].day) {
        c        = 0;
        flag = false;
        while (c < base.days.length && !flag) {
            if (base.days[c].day == rule[0].day) {
                base.days.splice(c, 1);
                flag = true;
            }
            c++;
        }
    } else {
        flag = false;
        if (rule.weekdays) {    
            flag = removeRuleFromWeek(rule.weekdays, rule[0].intervals)
        } else {
            flag = removeRuleFromWeek([0, 1, 2, 3, 4, 5, 6], rule[0].intervals)
        }
    }       
    return flag;
}

function createDateInterval(start, end) {
    let ret = [];
    let tmp = {};
    let c = 0;
    let weekday;    
    let day;
    let copyDay = {};;
    let dateParts;
    let flag = false;

    while(start.getTime() != end.getTime()) {              
        weekday = start.getDay();
        if (base[weekday]) {
            tmp = {    
                day: ("0" + (start.getMonth() + 1)).slice(-2) + '-' + ("0" + start.getDate()).slice(-2) + '-' + start.getFullYear(),
                intervals: base[weekday]
            };
        }  
        while (c < base.days.length && !flag) {            
            day = base.days[c].day;
            day = new Date(day);
            if (start.getTime() == day.getTime()) {   
                if (tmp.day) {
                    tmp.intervals = tmp.intervals.concat(base.days[c].intervals);
                    copyDay = JSON.parse(JSON.stringify(tmp));
                } else {
                    copyDay = JSON.parse(JSON.stringify(base.days[c]));
                }                  
                flag = true;               
            }
            c++;
        }  

        if (flag) {
            dateParts = copyDay.day.split('-');
            copyDay.day = dateParts[1] + '-' + dateParts[0] + '-' + dateParts[2];
            ret.push(copyDay); 
        } else if (tmp.day) {
            copyDay = JSON.parse(JSON.stringify(tmp));
            dateParts = copyDay.day.split('-');
            copyDay.day = dateParts[1] + '-' + dateParts[0] + '-' + dateParts[2];
            ret.push(copyDay);
        }

        start.setDate(start.getDate() + 1);
        tmp = {};   
        c = 0;
        flag = false;
    }
    return ret;
}

// Carregando bibliotecas
var express = require("express");
var app = express();
var bodyParser = require('body-parser');

// Adicionando capacidade de recuperar o body
app.use(bodyParser.json({ type: 'application/json' }));
app.use(bodyParser.urlencoded({ extended: true }));

// Rotas com suas respectivia acoes
app.listen(3000, () => {
 console.log("Servidor rodando na porta 3000");
});

app.get('/list-rules', (req, res) => {
    res.json(base.rules);
});

app.post("/create-rule", (req, res) => {
    let rule = req.body;
    let copyRule;
    if (rule.intervals) {
        if (rule.day && rule.weekdays) {
            res.status(400).json({
                status: "error",
                message: "Regra fora do padrao",
            });
            return;
        }
        
        if (rule.day) {
            base.days.push(rule);
        } else {
            loadWeekIntervals(rule);
        }

        base.sequence++;
        copyRule = JSON.parse(JSON.stringify(rule));
        copyRule.id = base.sequence;
        base.rules.push(copyRule);
        
        saveBase(base);
        res.status(200).json({
            status: "success",
            message: "Regra Cadastrada",
        });
    } else {
        res.status(400).json({
            status: "error",
            message: "Regra fora do padrao",
        });
    }
});

app.delete('/delete-rule/:id', (req, res) => {
    let id = req.params.id;
    let flag =  false;
    if (!isNaN(id)) {
        flag = removeRule(id);
        if (flag) {
            saveBase(base);
            res.status(200).json({
                status: "success",
                message: "Regra Excluida",
            });
        } else {
            res.status(406).json({
                status: "error",
                message: "Registro Nao Encontrado",
            });
        }
        
    } else {
        res.status(200).json({
            status: "error",
            message: "Requisicao Incorreta",
        });
    }    
});

app.post("/availability", (req, res) => {
    let ret = [];
    if (isValidDate(req.body.start.replace(/-/g, "/")) && isValidDate(req.body.end.replace(/-/g, "/"))) {
        let start = new Date(req.body.start);
        let end   = new Date(req.body.end);
        if (start.getTime() > end.getTime()) {
            res.status(400).json({
                status: "error",
                message: "Data Inicial Superior a Final",
            });
            return;
        }
        ret = createDateInterval(start, end);
        res.status(200).json(ret);
    } else {
        res.status(400).json({
            status: "error",
            message: "Datas Incorretas",
        });
        return;
    }
});